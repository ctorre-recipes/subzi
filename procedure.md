Procedure
=========

1. Add oil and garlic to the **pan**.
2. Chop tomatoes and put the in the hot oil.
3. Season with salt, red chilli powder and garam masala.
4. Slice potates into small cubes and add them to the mix.
5. Add water, **cover**, and occasionally add more water if it gets too reduced.
6. Cook for about 15 min.
7. Prepare bread or rice sides.
